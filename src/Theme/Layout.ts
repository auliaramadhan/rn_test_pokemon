import { StyleSheet, NativeModules, Platform, Dimensions } from 'react-native'
import { ThemeVariables } from './theme'

function LayoutFunc({ }: ThemeVariables) {
  const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : NativeModules.StatusBarManager.HEIGHT
  const Screen = Dimensions.get('window')
  return {
    ...StyleSheet.create({
      /* Column Layouts */
      column: {
        flexDirection: 'column',
      },
      columnReverse: {
        flexDirection: 'column-reverse',
      },
      colCenter: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      },
      colVCenter: {
        flexDirection: 'column',
        alignItems: 'center',
      },
      colHCenter: {
        flexDirection: 'column',
        justifyContent: 'center',
      },
      /* Row Layouts */
      row: {
        flexDirection: 'row',
      },
      rowReverse: {
        flexDirection: 'row-reverse',
      },
      rowCenter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
      },
      rowVCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
      },
      rowHCenter: {
        flexDirection: 'row',
        alignItems: 'center',
      },
      rowSpaceBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
      rowSpaceAround: {
        flexDirection: 'row',
        justifyContent: 'space-around',
      },
      rowSpaceEvenly: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
      },
      /* Default Layouts */
      center: {
        alignItems: 'center',
        justifyContent: 'center',
      },
      alignItemsCenter: {
        alignItems: 'center',
      },
      alignItemsStart: {
        alignItems: 'flex-start',
      },
      alignItemsStretch: {
        alignItems: 'stretch',
      },
      justifyContentCenter: {
        justifyContent: 'center',
      },
      justifyContentAround: {
        justifyContent: 'space-around',
      },
      justifyContentBetween: {
        justifyContent: 'space-between',
      },
      scrollSpaceAround: {
        flexGrow: 1,
        justifyContent: 'space-around',
      },
      scrollSpaceBetween: {
        flexGrow: 1,
        justifyContent: 'space-between',
      },
      selfStretch: {
        alignSelf: 'stretch',
      },
      selfCenter: {
        alignSelf: 'center',
      },
      selfEnd: {
        alignSelf: 'flex-end',
      },
      selfStar: {
        alignSelf: 'flex-start',
      },
      /* Sizes Layouts */
      fill: {
        flex: 1,
      },
      fullSize: {
        height: Screen.height,
        width: '100%',
      },
      fullWidth: {
        width: Screen.width,
      },
      ScreenWidth: {
        width: '100%',
      },
      fullHeight: {
        height: Screen.height,
      },
      fullScreen: {
        height: Screen.height,
        width: Screen.width,
      },
      bgRed: {
        backgroundColor: 'red',
      },
      noExpandStart: { flex: 0, alignSelf: 'flex-start' },
      noExpandCenter: { flex: 0, alignSelf: 'center' },
      absoluteCenter: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
      },
      absoluteHCenter: {
        position: 'absolute',
        left: 0,
        right: 0,
      },
      absoluteVCenter: {
        position: 'absolute',
        top: 0,
        bottom: 0,
      },
      /* Operation Layout */
      mirror: {
        transform: [{ scaleX: -1 }],
      },
      rotate90: {
        transform: [{ rotate: '90deg' }],
      },
      rotate90Inverse: {
        transform: [{ rotate: '-90deg' }],
      },
    }),
    getNumberWidth: Screen.width,
    getNumberhHight: Screen.height,
  }
}

export default LayoutFunc
