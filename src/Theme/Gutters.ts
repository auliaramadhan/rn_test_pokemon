import { StyleSheet, ViewStyle } from 'react-native'
import { ThemeVariables } from './theme'
import { MetricsSizes } from './Variables'

/**
 * Generate Styles depending on MetricsSizes vars availabled at ./Theme/Variables
 * Styles are like :
 * <size><direction><op>: {
 *    <op><direction>: <value>
 * }
 * where:
 * <size>: is the key of the variable included in MetricsSizes
 * <direction>: can be ['Bottom','Top','Right','Left','Horizontal','Vertical']
 * <op>: can be ['Margin', 'Padding']
 * <value>: is the value of the <size>
 */

// type Margins =
//   | 'BMargin'
//   | 'TMargin'
//   | 'RMargin'
//   | 'LMargin'
//   | 'VMargin'
//   | 'HMargin'
// type Paddings =
//   | 'BPadding'
//   | 'TPadding'
//   | 'RPadding'
//   | 'LPadding'
//   | 'VPadding'
//   | 'HPadding'

// type MarginKeys = `${keyof ThemeVariables['MetricsSizes']}${Margins}`
// type PaddingKeys = `${keyof ThemeVariables['MetricsSizes']}${Paddings}`

// type Gutters = {
//   [key in MarginKeys | PaddingKeys]: {
//     [k in string]: number
//   }
// }


// tambahab

const dataSize = [24,20,16,12,8,4,] as const
type type = 'margin' | 'padding'
type typeDirection = '' | 'B' | 'T' | 'R' | 'L' | 'V' | 'H'

// type typesize = keyof typeof dataSize
type typesize = typeof dataSize[number] 

type fooTambahan = `${type}${typesize}${typeDirection}`

type IGutter2 = { [P in fooTambahan]: ViewStyle }

type IGutter = IGutter2


let Gutters: IGutter = {} as IGutter

for (let size of dataSize) {
  Gutters[`margin${size}`] = {
    margin: size,
  }
  Gutters[`margin${size}B`] = {
    marginBottom: size,
  }
  Gutters[`margin${size}T`] = {
    marginTop: size,
  }
  Gutters[`margin${size}R`] = {
    marginRight: size,
  }
  Gutters[`margin${size}L`] = {
    marginLeft: size,
  }
  Gutters[`margin${size}V`] = {
    marginVertical: size,
  }
  Gutters[`margin${size}H`] = {
    marginHorizontal: size,
  }
  /* Paddings */
  Gutters[`padding${size}`] = {
    padding: size,
  }
  Gutters[`padding${size}B`] = {
    paddingBottom: size,
  }
  Gutters[`padding${size}T`] = {
    paddingTop: size,
  }
  Gutters[`padding${size}R`] = {
    paddingRight: size,
  }
  Gutters[`padding${size}L`] = {
    paddingLeft: size,
  }
  Gutters[`padding${size}V`] = {
    paddingVertical: size,
  }
  Gutters[`padding${size}H`] = {
    paddingHorizontal: size,
  }
}

export const Gutter = Gutters

function GuttersFunc({ MetricsSizes }: ThemeVariables): IGutter {
  return StyleSheet.create(
     Gutters,
  )
}

export default GuttersFunc

