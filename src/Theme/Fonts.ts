/**
 * This file contains all application's style relative to fonts
 */
import { StyleSheet } from 'react-native'
import { ThemeVariables } from './theme'

function FontsFunc({ FontSize, Colors }: ThemeVariables) {
  return StyleSheet.create({
    h1: {
      fontSize: FontSize.h1,
      color: Colors.colorText.primary,
    },
    h2: {
      fontWeight: 'bold',
      fontSize: FontSize.h2,
    },
    h3: {
      fontSize: FontSize.h3,
      color: Colors.colorText.primary,
    },
    h4: {
      fontSize: FontSize.h4,
      color: Colors.colorText.primary,
    },
    h5: {
      fontSize: FontSize.h5,
      color: Colors.colorText.primary,
    },
    h6: {
      fontSize: FontSize.h6,
    },
    title: {
      fontSize: FontSize.title,
      color: Colors.colorText.primary,
    },
    titleBold: {
      fontWeight: 'bold',
      fontSize: FontSize.title,
      color: Colors.colorText.primary,
    },
    headerBold: {
      fontWeight: 'bold',
      fontSize: 22,
      color: Colors.colorText.primary,
    },
    title18: {
      fontSize: 18,
      color: Colors.colorText.primary,
    },
    title16: {
      fontWeight: 'bold',
      fontSize: 16,
      color: Colors.colorText.primary,
    },
    title18Bold: {
      fontWeight: 'bold',
      fontSize: 18,
      color: Colors.colorText.primary,
    },
    subtitle: {
      fontSize: 14,
      color: Colors.colorText.secondary,
    },
    normal: {
      fontSize: 14,
      color: Colors.colorText.primary,
    },
    normalBold: {
      fontWeight: 'bold',
      fontSize: 14,
      color: Colors.colorText.primary,
    },
    description: {
      fontSize: FontSize.medium,
      color: Colors.colorText.primary,
    },
    body: {
      fontSize: FontSize.text14,
      color: Colors.colorText.primary,
    },
    bodyGrey: {
      fontSize: FontSize.text14,
      color: Colors.colorText.secondary,
    },
    bodyBold: {
      fontWeight: 'bold',
      fontSize: FontSize.text14,
      color: Colors.colorText.primary,
    },
    caption12: {
      fontSize: FontSize.medium,
      color: Colors.colorText.secondary,
    },
    caption10: {
      fontSize: FontSize.tiny10,
      color: Colors.colorText.secondary,
    },
    input: {
      fontWeight: 'bold',
      color: Colors.colorText.secondary,
      fontSize: FontSize.input,
    },
    body12: {
      fontSize: FontSize.small,
      color: Colors.colorText.primary,
    },
    body12Bold: {
      fontSize: FontSize.small,
      fontWeight: 'bold',
      color: Colors.colorText.primary,
    },
    subtitle16: {
      fontSize: 16,
      color: Colors.colorText.secondary,
    },
    subtitle14: {
      fontSize: 14,
      color: Colors.colorText.secondary,
    },
    // basic
    primary: {
      color: Colors.primary,
    },
    black: {
      color: Colors.colorText.primary,
    },
    white: {
      color: Colors.white,
    },
    grey: {
      color: Colors.colorText.secondary,
    },
    bold: {
      fontWeight: 'bold'
    },
    text12: {
      fontSize: FontSize.text12,
    },
    text14: {
      fontSize: FontSize.text14,
    },
    text16: {
      fontSize: FontSize.text16,
    },
    text18: {
      fontSize: FontSize.text18,
    },
    text20: {
      fontSize: FontSize.text20,
    },

    textCenter: {
      textAlign: 'center',
    },
    textJustify: {
      textAlign: 'justify',
    },
    textLeft: {
      textAlign: 'left',
    },
    textRight: {
      textAlign: 'right',
    },
  })
}


export default FontsFunc