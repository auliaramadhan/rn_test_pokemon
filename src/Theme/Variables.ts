/**
 * This file contains the application's variables.
 *
 * Define color, sizes, etc. here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

/**
 * Colors
 */
export const Colors = {
  // Example colors:
  transparent: 'rgba(0,0,0,0)',
  white: '#ffffff',
  text: '#212529',
  primary: '#E14032',
  primaryShadow: 'rgba(235, 87, 87, 0.2)',
  divider: '#F0F0F0',
  border: '#483F53',
  borderList: '#F4F5F6',
  bglayout: '#F6F6F7',
  bglayout2: '#A6A9B010',
  loadingBg: '#EFEFEF',
  loadingHightlight: '#FBFBFB',
  greyDark: '#858585',
  blackText: '#333333',
  blackText60: '#33333360',

  // background
  background: '#1F0808',
  bgInput: '#F4F4F5',
  bgInput2: '#a6a9b012',
  bgPrimary: '#F4F4F5',
  bgYellow: '#FBEC97',
  bgModal: '#00000060',
  bgGrey: '#F4F4F5',
  bgBlack: '#151B26',
  bgForm: 'rgba(166, 169, 176, 0.12)',

  // normal color
  success: '#28a745',
  error: '#dc3545',
  warning: '#ffc107',
  info: '#0dcaf0',
  iconColor: '#A6A9B0',

  name: {
    greenProgress: '119e27',
    backgroundAlt: '#F7F8FB',
    border1: 'rgba(0, 0, 0, 0.1)',
    greenAlt: '#14AA2B',
    yellow: '#F2994A',
    grey: '#A6A9B0',
    dark: '#222222',
    green: '#0FBF76',
    greenPalma: '#14AA2B',
    red: '#E14032',
    orange: '#F2994A',
    softOrange: '#FFBE5C',
    blue: '#00aaff',
    white: '#eeeeee',
    black: '#111111',
  },
  // color text
  colorText: {
    primary: '#333',
    secondary: '#8a8d8e',
    black: '#333',
    white: '#eee',
    grey: '#8a8d8e',
  },
}

export const NavigationColors = {
  primary: Colors.primary,
}

/**
 * FontSize
 */
export const FontSize = {
  body : 14,
  // small: 16,
  // regular: 20,
  large: 40,
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  title: 24,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 10,
  tiny8: 8,
  tiny10: 10,
  text8: 8,
  text11: 11,
  text12: 12,
  text13: 13,
  text14: 14,
  text16: 16,
  text18: 18,
  text20: 20,
  text22: 22,
  text24: 24,
  text26: 26,
}

/**
 * Metrics Sizes
 */
const tiny = 5 // 10
const small = tiny * 2 // 10
const regular = tiny * 3 // 15
const large = regular * 2 // 30
export const MetricsSizes = {
  tiny,
  small,
  regular,
  large,
}

const Variables = {
  Colors,
  NavigationColors,
  FontSize,
  MetricsSizes,
}

export default Variables
