import React from 'react'
import LoginScreen from "@/Containers/LoginScreen/LoginScreen";
import { MainStack } from './utils';
import RegisterScreen from '@/Containers/RegisterScreen/RegisterScreen';

const AuthNav = (
  <>
    <MainStack.Screen
      name="Login"
      options={{
        headerShown: false,
      }}
      component={LoginScreen}
    />
    <MainStack.Screen
      name="Register"
      options={{
        headerShown: false,
      }}
      component={RegisterScreen}
    />
  </>
)

export default AuthNav