import React from 'react'
import { SafeAreaView, StatusBar } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import { ExampleContainer, StartupContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import MainNavigator from './Main'
import { MainStack, navigationRef } from './utils'
import AuthNav from './authNav'
import PokemonDetailScreen from '@/Containers/PokemonDetail/PokemonDetailScreen'

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, darkMode, NavigationTheme } = useTheme()
  const { colors } = NavigationTheme

  return (
    <SafeAreaView style={[Layout.fill, { backgroundColor: colors.card }]}>
      <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
        <StatusBar
          barStyle={darkMode ? 'light-content' : 'dark-content'}
          // TODO
          // backgroundColor={backgroundStyle.backgroundColor}
        />
        <MainStack.Navigator>
          <MainStack.Screen
            name="Startup"
            component={StartupContainer}
            options={{
              animationEnabled: false,
              headerShown: false,
            }}/>
          {AuthNav}
          <MainStack.Screen
            name="Main"
            component={MainNavigator}
            options={{
              animationEnabled: false,
              headerShown: false,
            }}
          />
          <MainStack.Screen
            name="PokemonDetail"
            component={PokemonDetailScreen}
            options={{
              headerTransparent: true,
            }}
          />
          <MainStack.Screen
            name="Example"
            component={ExampleContainer}
            options={{
              animationEnabled: false,
            }}
          />
        </MainStack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  )
}

export { ApplicationNavigator }
