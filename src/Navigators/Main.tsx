import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { ExampleContainer } from '@/Containers'
import HomeScreen from '@/Containers/HomeScreen/HomeScreen'
import MaterialIcons from '@/Components/MaterialIcons'
import { useTheme } from '@/Hooks'
import { View } from 'react-native'
import { Gutter } from '@/Theme/Gutters'
import MyPokemonScreen from '@/Containers/MyPokemon/MyPokemonScreen'

const Tab = createBottomTabNavigator()

// @refresh reset
const MainNavigator = () => {
  const { Colors, Fonts, Layout, Common, darkMode } = useTheme()
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: Colors.primary,
        tabBarInactiveTintColor: Colors.colorText.secondary,
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="home" size={30} color={color} />
          ),
          tabBarShowLabel: false,
          // headerTitleStyle: Fonts.title18
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="MyPokemon"
        component={MyPokemonScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="star" size={30} color={color} />
          ),
          tabBarShowLabel: false,
          // headerTitleStyle: Fonts.title18
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  )
}

export default MainNavigator
