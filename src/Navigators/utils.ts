import { PokemonData } from '../types/api/pokemonList';
/**
 * Used to navigating without the navigation prop
 * @see https://reactnavigation.org/docs/navigating-without-navigation-prop/
 *
 * You can add other navigation functions that you need and export them
 */
import {
  CommonActions,
  CompositeNavigationProp,
  createNavigationContainerRef,
} from '@react-navigation/native'
import {
  createStackNavigator,
  StackNavigationProp,
  StackScreenProps,
} from '@react-navigation/stack'

type RootStackParamList = {
  Startup: undefined
  Example: {
    example?: string
  }
  Main: undefined
  Login: undefined
  Register: undefined
  PokemonDetail: PokemonData
}

export type RootStackScreenProps<T extends keyof RootStackParamList> =
  StackScreenProps<RootStackParamList, T>

export const navigationRef = createNavigationContainerRef<RootStackParamList>()
export const MainStack = createStackNavigator()

export function navigate<T extends keyof RootStackParamList>(
  name: T,
  params: RootStackParamList[T],
) {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name, params)
  }
}

export function navigateAndReset(routes = [], index = 0) {
  if (navigationRef.isReady()) {
    navigationRef.dispatch(
      CommonActions.reset({
        index,
        routes,
      }),
    )
  }
}

export function navigateAndSimpleReset(
  name: keyof RootStackParamList,
  index = 0,
) {
  if (navigationRef.isReady()) {
    navigationRef.dispatch(
      CommonActions.reset({
        index,
        routes: [{ name }],
      }),
    )
  }
}
