export interface PokemonList {
  count: number
  next: string
  previous: string
  results: PokemonData[]
}

export interface PokemonData {
  name: string
  url: string
  id: number
}

/* 
export function toPokemonList(json: string): PokemonList {
  return JSON.parse(json)
}
// Converts JSON strings to/from your types
export function pokemonListToJson(value: PokemonList): string {
  return JSON.stringify(value)
} 
*/
