// To parse this data:
//
//   import { Convert, Pokemon } from "./file"
//
//   const pokemon = Convert.toPokemon(json)
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface Pokemon {
  abilities: Ability[]
  baseExperience: number
  forms: Species[]
  gameIndices: GameIndex[]
  height: number
  heldItems: HeldItem[]
  id: number
  isDefault: boolean
  locationAreaEncounters: string
  moves: Move[]
  name: string
  order: number
  pastTypes: any[]
  species: Species
  sprites: Sprites
  stats: Stat[]
  types: Type[]
  weight: number
}

export interface Ability {
  ability: Species
  isHidden: boolean
  slot: number
}

export interface Species {
  name: string
  url: string
}

export interface GameIndex {
  gameIndex: number
  version: Species
}

export interface HeldItem {
  item: Species
  versionDetails: VersionDetail[]
}

export interface VersionDetail {
  rarity: number
  version: Species
}

export interface Move {
  move: Species
  versionGroupDetails: VersionGroupDetail[]
}

export interface VersionGroupDetail {
  levelLearnedAt: number
  moveLearnMethod: Species
  versionGroup: Species
}

export interface GenerationV {
  blackWhite: Sprites
}

export interface GenerationIv {
  diamondPearl: Sprites
  heartgoldSoulsilver: Sprites
  platinum: Sprites
}

export interface Versions {
  generationI: GenerationI
  generationIi: GenerationIi
  generationIii: GenerationIii
  generationIv: GenerationIv
  generationV: GenerationV
  generationVi: { [key: string]: Home }
  generationVii: GenerationVii
  generationViii: GenerationViii
}

export interface Sprites {
  backDefault: string
  backFemale: null
  backShiny: string
  backShinyFemale: null
  frontDefault: string
  frontFemale: null
  frontShiny: string
  frontShinyFemale: null
  other?: Other
  versions?: Versions
  animated?: Sprites
}

export interface GenerationI {
  redBlue: RedBlue
  yellow: RedBlue
}

export interface RedBlue {
  backDefault: string
  backGray: string
  backTransparent: string
  frontDefault: string
  frontGray: string
  frontTransparent: string
}

export interface GenerationIi {
  crystal: Crystal
  gold: Gold
  silver: Gold
}

export interface Crystal {
  backDefault: string
  backShiny: string
  backShinyTransparent: string
  backTransparent: string
  frontDefault: string
  frontShiny: string
  frontShinyTransparent: string
  frontTransparent: string
}

export interface Gold {
  backDefault: string
  backShiny: string
  frontDefault: string
  frontShiny: string
  frontTransparent?: string
}

export interface GenerationIii {
  emerald: Emerald
  fireredLeafgreen: Gold
  rubySapphire: Gold
}

export interface Emerald {
  frontDefault: string
  frontShiny: string
}

export interface Home {
  frontDefault: string
  frontFemale: null
  frontShiny: string
  frontShinyFemale: null
}

export interface GenerationVii {
  icons: DreamWorld
  ultraSunUltraMoon: Home
}

export interface DreamWorld {
  frontDefault: string
  frontFemale: null
}

export interface GenerationViii {
  icons: DreamWorld
}

export interface Other {
  dreamWorld: DreamWorld
  home: Home
  officialArtwork: OfficialArtwork
}

export interface OfficialArtwork {
  frontDefault: string
}

export interface Stat {
  baseStat: number
  effort: number
  stat: Species
}

export interface Type {
  slot: number
  type: Species
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export function toPokemon(json: string): Pokemon {
  return cast(JSON.parse(json), r('Pokemon'))
}

export function pokemonToJson(value: Pokemon): string {
  return JSON.stringify(uncast(value, r('Pokemon')), null, 2)
}

function invalidValue(typ: any, val: any, key: any = ''): never {
  if (key) {
    throw Error(
      `Invalid value for key "${key}". Expected type ${JSON.stringify(
        typ,
      )} but got ${JSON.stringify(val)}`,
    )
  }
  throw Error(
    `Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`,
  )
}

function jsonToJSProps(typ: any): any {
  if (typ.jsonToJS === undefined) {
    const map: any = {}
    typ.props.forEach((p: any) => (map[p.json] = { key: p.js, typ: p.typ }))
    typ.jsonToJS = map
  }
  return typ.jsonToJS
}

function jsToJSONProps(typ: any): any {
  if (typ.jsToJSON === undefined) {
    const map: any = {}
    typ.props.forEach((p: any) => (map[p.js] = { key: p.json, typ: p.typ }))
    typ.jsToJSON = map
  }
  return typ.jsToJSON
}

function transform(val: any, typ: any, getProps: any, key: any = ''): any {
  function transformPrimitive(typ: string, val: any): any {
    if (typeof typ === typeof val) return val
    return invalidValue(typ, val, key)
  }

  function transformUnion(typs: any[], val: any): any {
    // val must validate against one typ in typs
    const l = typs.length
    for (let i = 0; i < l; i++) {
      const typ = typs[i]
      try {
        return transform(val, typ, getProps)
      } catch (_) {}
    }
    return invalidValue(typs, val)
  }

  function transformEnum(cases: string[], val: any): any {
    if (cases.indexOf(val) !== -1) return val
    return invalidValue(cases, val)
  }

  function transformArray(typ: any, val: any): any {
    // val must be an array with no invalid elements
    if (!Array.isArray(val)) return invalidValue('array', val)
    return val.map(el => transform(el, typ, getProps))
  }

  function transformDate(val: any): any {
    if (val === null) {
      return null
    }
    const d = new Date(val)
    if (isNaN(d.valueOf())) {
      return invalidValue('Date', val)
    }
    return d
  }

  function transformObject(
    props: { [k: string]: any },
    additional: any,
    val: any,
  ): any {
    if (val === null || typeof val !== 'object' || Array.isArray(val)) {
      return invalidValue('object', val)
    }
    const result: any = {}
    Object.getOwnPropertyNames(props).forEach(key => {
      const prop = props[key]
      const v = Object.prototype.hasOwnProperty.call(val, key)
        ? val[key]
        : undefined
      result[prop.key] = transform(v, prop.typ, getProps, prop.key)
    })
    Object.getOwnPropertyNames(val).forEach(key => {
      if (!Object.prototype.hasOwnProperty.call(props, key)) {
        result[key] = transform(val[key], additional, getProps, key)
      }
    })
    return result
  }

  if (typ === 'any') return val
  if (typ === null) {
    if (val === null) return val
    return invalidValue(typ, val)
  }
  if (typ === false) return invalidValue(typ, val)
  while (typeof typ === 'object' && typ.ref !== undefined) {
    typ = typeMap[typ.ref]
  }
  if (Array.isArray(typ)) return transformEnum(typ, val)
  if (typeof typ === 'object') {
    return typ.hasOwnProperty('unionMembers')
      ? transformUnion(typ.unionMembers, val)
      : typ.hasOwnProperty('arrayItems')
      ? transformArray(typ.arrayItems, val)
      : typ.hasOwnProperty('props')
      ? transformObject(getProps(typ), typ.additional, val)
      : invalidValue(typ, val)
  }
  // Numbers can be parsed by Date but shouldn't be.
  if (typ === Date && typeof val !== 'number') return transformDate(val)
  return transformPrimitive(typ, val)
}

function cast<T>(val: any, typ: any): T {
  return transform(val, typ, jsonToJSProps)
}

function uncast<T>(val: T, typ: any): any {
  return transform(val, typ, jsToJSONProps)
}

function a(typ: any) {
  return { arrayItems: typ }
}

function u(...typs: any[]) {
  return { unionMembers: typs }
}

function o(props: any[], additional: any) {
  return { props, additional }
}

function m(additional: any) {
  return { props: [], additional }
}

function r(name: string) {
  return { ref: name }
}

const typeMap: any = {
  Pokemon: o(
    [
      { json: 'abilities', js: 'abilities', typ: a(r('Ability')) },
      { json: 'base_experience', js: 'baseExperience', typ: 0 },
      { json: 'forms', js: 'forms', typ: a(r('Species')) },
      { json: 'game_indices', js: 'gameIndices', typ: a(r('GameIndex')) },
      { json: 'height', js: 'height', typ: 0 },
      { json: 'held_items', js: 'heldItems', typ: a(r('HeldItem')) },
      { json: 'id', js: 'id', typ: 0 },
      { json: 'is_default', js: 'isDefault', typ: true },
      {
        json: 'location_area_encounters',
        js: 'locationAreaEncounters',
        typ: '',
      },
      { json: 'moves', js: 'moves', typ: a(r('Move')) },
      { json: 'name', js: 'name', typ: '' },
      { json: 'order', js: 'order', typ: 0 },
      { json: 'past_types', js: 'pastTypes', typ: a('any') },
      { json: 'species', js: 'species', typ: r('Species') },
      { json: 'sprites', js: 'sprites', typ: r('Sprites') },
      { json: 'stats', js: 'stats', typ: a(r('Stat')) },
      { json: 'types', js: 'types', typ: a(r('Type')) },
      { json: 'weight', js: 'weight', typ: 0 },
    ],
    false,
  ),
  Ability: o(
    [
      { json: 'ability', js: 'ability', typ: r('Species') },
      { json: 'is_hidden', js: 'isHidden', typ: true },
      { json: 'slot', js: 'slot', typ: 0 },
    ],
    false,
  ),
  Species: o(
    [
      { json: 'name', js: 'name', typ: '' },
      { json: 'url', js: 'url', typ: '' },
    ],
    false,
  ),
  GameIndex: o(
    [
      { json: 'game_index', js: 'gameIndex', typ: 0 },
      { json: 'version', js: 'version', typ: r('Species') },
    ],
    false,
  ),
  HeldItem: o(
    [
      { json: 'item', js: 'item', typ: r('Species') },
      {
        json: 'version_details',
        js: 'versionDetails',
        typ: a(r('VersionDetail')),
      },
    ],
    false,
  ),
  VersionDetail: o(
    [
      { json: 'rarity', js: 'rarity', typ: 0 },
      { json: 'version', js: 'version', typ: r('Species') },
    ],
    false,
  ),
  Move: o(
    [
      { json: 'move', js: 'move', typ: r('Species') },
      {
        json: 'version_group_details',
        js: 'versionGroupDetails',
        typ: a(r('VersionGroupDetail')),
      },
    ],
    false,
  ),
  VersionGroupDetail: o(
    [
      { json: 'level_learned_at', js: 'levelLearnedAt', typ: 0 },
      { json: 'move_learn_method', js: 'moveLearnMethod', typ: r('Species') },
      { json: 'version_group', js: 'versionGroup', typ: r('Species') },
    ],
    false,
  ),
  GenerationV: o(
    [{ json: 'black-white', js: 'blackWhite', typ: r('Sprites') }],
    false,
  ),
  GenerationIv: o(
    [
      { json: 'diamond-pearl', js: 'diamondPearl', typ: r('Sprites') },
      {
        json: 'heartgold-soulsilver',
        js: 'heartgoldSoulsilver',
        typ: r('Sprites'),
      },
      { json: 'platinum', js: 'platinum', typ: r('Sprites') },
    ],
    false,
  ),
  Versions: o(
    [
      { json: 'generation-i', js: 'generationI', typ: r('GenerationI') },
      { json: 'generation-ii', js: 'generationIi', typ: r('GenerationIi') },
      { json: 'generation-iii', js: 'generationIii', typ: r('GenerationIii') },
      { json: 'generation-iv', js: 'generationIv', typ: r('GenerationIv') },
      { json: 'generation-v', js: 'generationV', typ: r('GenerationV') },
      { json: 'generation-vi', js: 'generationVi', typ: m(r('Home')) },
      { json: 'generation-vii', js: 'generationVii', typ: r('GenerationVii') },
      {
        json: 'generation-viii',
        js: 'generationViii',
        typ: r('GenerationViii'),
      },
    ],
    false,
  ),
  Sprites: o(
    [
      { json: 'back_default', js: 'backDefault', typ: '' },
      { json: 'back_female', js: 'backFemale', typ: null },
      { json: 'back_shiny', js: 'backShiny', typ: '' },
      { json: 'back_shiny_female', js: 'backShinyFemale', typ: null },
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_female', js: 'frontFemale', typ: null },
      { json: 'front_shiny', js: 'frontShiny', typ: '' },
      { json: 'front_shiny_female', js: 'frontShinyFemale', typ: null },
      { json: 'other', js: 'other', typ: u(undefined, r('Other')) },
      { json: 'versions', js: 'versions', typ: u(undefined, r('Versions')) },
      { json: 'animated', js: 'animated', typ: u(undefined, r('Sprites')) },
    ],
    false,
  ),
  GenerationI: o(
    [
      { json: 'red-blue', js: 'redBlue', typ: r('RedBlue') },
      { json: 'yellow', js: 'yellow', typ: r('RedBlue') },
    ],
    false,
  ),
  RedBlue: o(
    [
      { json: 'back_default', js: 'backDefault', typ: '' },
      { json: 'back_gray', js: 'backGray', typ: '' },
      { json: 'back_transparent', js: 'backTransparent', typ: '' },
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_gray', js: 'frontGray', typ: '' },
      { json: 'front_transparent', js: 'frontTransparent', typ: '' },
    ],
    false,
  ),
  GenerationIi: o(
    [
      { json: 'crystal', js: 'crystal', typ: r('Crystal') },
      { json: 'gold', js: 'gold', typ: r('Gold') },
      { json: 'silver', js: 'silver', typ: r('Gold') },
    ],
    false,
  ),
  Crystal: o(
    [
      { json: 'back_default', js: 'backDefault', typ: '' },
      { json: 'back_shiny', js: 'backShiny', typ: '' },
      { json: 'back_shiny_transparent', js: 'backShinyTransparent', typ: '' },
      { json: 'back_transparent', js: 'backTransparent', typ: '' },
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_shiny', js: 'frontShiny', typ: '' },
      { json: 'front_shiny_transparent', js: 'frontShinyTransparent', typ: '' },
      { json: 'front_transparent', js: 'frontTransparent', typ: '' },
    ],
    false,
  ),
  Gold: o(
    [
      { json: 'back_default', js: 'backDefault', typ: '' },
      { json: 'back_shiny', js: 'backShiny', typ: '' },
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_shiny', js: 'frontShiny', typ: '' },
      {
        json: 'front_transparent',
        js: 'frontTransparent',
        typ: u(undefined, ''),
      },
    ],
    false,
  ),
  GenerationIii: o(
    [
      { json: 'emerald', js: 'emerald', typ: r('Emerald') },
      { json: 'firered-leafgreen', js: 'fireredLeafgreen', typ: r('Gold') },
      { json: 'ruby-sapphire', js: 'rubySapphire', typ: r('Gold') },
    ],
    false,
  ),
  Emerald: o(
    [
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_shiny', js: 'frontShiny', typ: '' },
    ],
    false,
  ),
  Home: o(
    [
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_female', js: 'frontFemale', typ: null },
      { json: 'front_shiny', js: 'frontShiny', typ: '' },
      { json: 'front_shiny_female', js: 'frontShinyFemale', typ: null },
    ],
    false,
  ),
  GenerationVii: o(
    [
      { json: 'icons', js: 'icons', typ: r('DreamWorld') },
      { json: 'ultra-sun-ultra-moon', js: 'ultraSunUltraMoon', typ: r('Home') },
    ],
    false,
  ),
  DreamWorld: o(
    [
      { json: 'front_default', js: 'frontDefault', typ: '' },
      { json: 'front_female', js: 'frontFemale', typ: null },
    ],
    false,
  ),
  GenerationViii: o(
    [{ json: 'icons', js: 'icons', typ: r('DreamWorld') }],
    false,
  ),
  Other: o(
    [
      { json: 'dream_world', js: 'dreamWorld', typ: r('DreamWorld') },
      { json: 'home', js: 'home', typ: r('Home') },
      {
        json: 'official-artwork',
        js: 'officialArtwork',
        typ: r('OfficialArtwork'),
      },
    ],
    false,
  ),
  OfficialArtwork: o(
    [{ json: 'front_default', js: 'frontDefault', typ: '' }],
    false,
  ),
  Stat: o(
    [
      { json: 'base_stat', js: 'baseStat', typ: 0 },
      { json: 'effort', js: 'effort', typ: 0 },
      { json: 'stat', js: 'stat', typ: r('Species') },
    ],
    false,
  ),
  Type: o(
    [
      { json: 'slot', js: 'slot', typ: 0 },
      { json: 'type', js: 'type', typ: r('Species') },
    ],
    false,
  ),
}
