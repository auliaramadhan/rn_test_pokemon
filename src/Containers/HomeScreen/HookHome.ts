import useArray from '@/Hooks/useArray'
import useUpdateEffect from '@/Hooks/useUpdateEffect'
import { useFetchPokemonQuery } from '@/Services/modules/pokemon'
import { PokemonData } from '@/types/api/pokemonList'
import React from 'react'

export default function useHookHome() {
  const [currentPage, setcurrentPage] = React.useState(1)
  const listPokemon = useArray<PokemonData>([])

  const { isLoading, isSuccess, data, isFetching } = useFetchPokemonQuery({
    limit: 20,
    offset: (currentPage * 20) - 20,
  })
  const query = { isLoading, isSuccess, isFetching }

  useUpdateEffect(() => {
    if (currentPage === 1) {
      listPokemon.clear()
    }
    console.log(isLoading, isSuccess, isFetching, currentPage)
    listPokemon.addAll(data?.results ?? [])
  }, [data])

  const fetchNext = (reset: boolean) => {
    if (isFetching) { return }
    if (reset) {
      setcurrentPage(1)
    } else {
      setcurrentPage(v => v + 1)
    }
  }

  return { query, listPokemon: listPokemon.array, fetchNext }
}
