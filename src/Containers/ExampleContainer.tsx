import React, { useState } from 'react'
import { View, ScrollView, SafeAreaView, Text, Image } from 'react-native'
import { useDispatch } from 'react-redux'
import { useTheme } from '@/Hooks'
import { changeTheme, ThemeState } from '@/Store/Theme'
import { logOut } from '@/Store/auth'
import { Gutter } from '@/Theme/Gutters'
import { RootStackScreenProps } from '@/Navigators/utils'
import { ButtonPrimary } from '@/Components'

const ExampleContainer: React.FC<RootStackScreenProps<'Example'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors, darkMode } = useTheme()

  const [userId, setUserId] = useState('9')
  const dispatch = useDispatch()
  // const { data, isSuccess, isLoading, isFetching, error }= useFetchOneQuery()

  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      {/* <Image source={Images.bgEllipse} style={styles.overlay} /> */}
      <View style={Layout.fill}>
        <ScrollView contentContainerStyle={[Gutter.padding16]}>
          <Text style={Fonts.body}>{route.params.example}</Text>
          <ButtonPrimary
            text="sadas"
            onPress={() => {
              console.log('first')
            }}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default ExampleContainer
