import React from 'react'
import {
  View,
  SafeAreaView,
  Text,
  FlatList,
  Image,
  ViewStyle,
  Pressable,
} from 'react-native'
import { useTheme } from '@/Hooks'
import { Gutter } from '@/Theme/Gutters'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { RootStackScreenProps } from '../../Navigators/utils'
import { useRef, useEffect, useCallback } from 'react'
import apiUrl from '@/Config/apiUrl'
import { useFetchPokemonQuery } from '@/Services/modules/pokemon'
import { PokemonData } from '@/types/api/pokemonList'
import { Images } from '../../Theme/Images'
import { Spacer } from '@/Components'
import { ActivityIndicator } from 'react-native'
import { useSelector } from 'react-redux'
import { rootState } from '@/Store'

const MyPokemonScreen: React.FC<RootStackScreenProps<'Main'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors } = useTheme()

  const listRef = useRef<FlatList>(null)
  const listPokemon = useSelector((state: rootState) => state.myPokemon.list)

  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      <FlatList<PokemonData>
        // ListHeaderComponent={
        //   <>
        //     <Text style={Fonts.h4}>Pokemon List</Text>
        //   </>
        // }
        ref={listRef}
        showsVerticalScrollIndicator={false}
        data={listPokemon ?? []}
        numColumns={2}
        horizontal={false}
        renderItem={({ index, item }) => (
          <View style={[Layout.fill, Gutter.padding8]}>
            <TouchableOpacity
              onPress={() => {
                navigation.push('PokemonDetail', item)
              }}
            >
              <View style={[Common.cardBorder, Layout.center]}>
                <Image
                  source={Images.pokeball}
                  style={[
                    { position:'absolute', right: -20, bottom: -20 },
                    { height: 80, width: 80 },
                  ]}
                />
                <View style={[Layout.column, Gutter.padding12]}>
                  <Text style={[Fonts.title18]}>{item.name}</Text>
                  <Image
                    resizeMode="contain"
                    style={Common.circle(60)}
                    source={{
                      uri: `${apiUrl.BASE_IMG_POKE}/${item.id}.png`,
                    }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
      />
    </SafeAreaView>
  )
}

export default MyPokemonScreen
