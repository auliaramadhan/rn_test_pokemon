import React, { useState } from 'react'
import { View, Text, SafeAreaView, ScrollView } from 'react-native'
import { ButtonPrimary, InputBorder, Spacer } from '@/Components'
import { useTheme } from '@/Hooks'
import { Gutter } from '@/Theme/Gutters'
import { RootStackScreenProps } from '../../Navigators/utils'
import MaterialIcons from '@/Components/MaterialIcons'
import useDebounce from '@/Hooks/useDebounce'
import Snackbar from 'react-native-snackbar'

const RegisterScreen: React.FC<RootStackScreenProps<'Register'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Gutters, Layout, Colors } = useTheme()

  const [showPass, setShowPass] = useState(false)
  const [password, setPassword] = useState('')
  const [repassword, setRePassword] = useState('')
  const [name, setNama] = useState('')
  const [email, setEmail] = useState('')
  const [noHp, setNoHp] = useState('')
  const [alamat, setAlamat] = useState('')
  const [listAlamat, setListalamat] = useState([])

  return (
    <SafeAreaView style={[Layout.fill]}>
      {/* <Spacer height={20} /> */}
      <View style={[Layout.fill, Common.backgroundPrimary]}>
        <ScrollView contentContainerStyle={[Gutters.padding16]}>
          <Spacer height={20} />
          <View>
            <Text style={[Fonts.title, Gutters.margin16B]}>Register</Text>
            <Text style={[Fonts.subtitle]}>
              Isi data pendaftaran. Ini akan memakan waktu beberapa menit
            </Text>
            <Spacer height={12} />

            <Text style={[Fonts.bodyBold]}>Nama Lengkap</Text>
            <Spacer height={8} />
            <InputBorder placeholder="Joshua" onChangeText={setNama} />
            <Spacer height={12} />

            <Text style={[Fonts.bodyBold]}>Email</Text>
            <Spacer height={8} />
            <InputBorder
              keyboardType="email-address"
              placeholder="example@email.com"
              onChangeText={setEmail}
            />
            <Spacer height={12} />

            <Text style={[Fonts.bodyBold]}>No. HP</Text>
            <Spacer height={8} />
            <InputBorder
              keyboardType="phone-pad"
              placeholder="+62"
              onChangeText={setNoHp}
            />
            <Spacer height={12} />

            <Text style={[Fonts.bodyBold]}>Alamat</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="jl. sudirman no x"
              onChangeText={setAlamat}
            />
            <Spacer height={12} />

            <Text style={[Fonts.bodyBold]}>Password</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="Password"
              secureTextEntry={!showPass}
              onChangeText={setPassword}
              suffix={
                <MaterialIcons
                  name={showPass ? 'visibility-off' : 'visibility'}
                  // name=''
                  size={20}
                  color={Colors.colorText.secondary}
                  style={Gutter.padding8H}
                  onPress={() => {
                    setShowPass(v => !v)
                  }}
                />
              }
            />
            <Spacer height={12} />

            <Text style={[Fonts.bodyBold]}>Password Again</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="Password"
              secureTextEntry={!showPass}
              onChangeText={setRePassword}
              suffix={
                <MaterialIcons
                  name={showPass ? 'visibility-off' : 'visibility'}
                  size={20}
                  color={Colors.colorText.secondary}
                  style={Gutter.padding8H}
                  onPress={() => {
                    setShowPass(v => !v)
                  }}
                />
              }
            />
            <Spacer height={12} />
            {/* geocodernya masih bermaasalah
            <Text>
              {JSON.stringify(listAlamat.length > 0 && listAlamat[0]?.country)}
              {JSON.stringify(listAlamat)}
            </Text>
             */}
            <Spacer height={24} />
            <ButtonPrimary
              text="Daftar Sekarang"
              disabled={
                !(
                  (!!password || !!name || !!email || !!noHp || !!alamat) &&
                  password === repassword
                )
              }
              onPress={() => {
                Snackbar.show({
                  text: 'Password tidak sesua',
                  backgroundColor: Colors.bgModal,
                  textColor: 'white',
                  duration: Snackbar.LENGTH_SHORT,
                  action: {
                    text: 'Done',
                    onPress() {
                      navigation.goBack()
                    },
                  },
                })

                console.log('first')
              }}
            />
            <Spacer height={12} />
          </View>
        </ScrollView>
        {/* <ActivityIndicator style={Layout.absoluteCenter} size={40} /> */}
      </View>
    </SafeAreaView>
  )
}

export default RegisterScreen
