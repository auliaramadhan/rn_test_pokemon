import React, { useState } from 'react'
import { View, Text, SafeAreaView, ScrollView } from 'react-native'
import { useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { ButtonPrimary, InputBorder, Spacer } from '@/Components'
import { useTheme } from '@/Hooks'
import TextButton from '@/Components/button/ButtonText'
import { Gutter } from '@/Theme/Gutters'
import { RootStackScreenProps } from '../../Navigators/utils'
import Snackbar from 'react-native-snackbar'
import { login } from '@/Store/auth'
import { faker } from '@faker-js/faker'
import MaterialIcons from '@/Components/MaterialIcons'

const LoginScreen: React.FC<RootStackScreenProps<'Login'>> = ({
  navigation,
  route,
}) => {
  const { t } = useTranslation()
  const { Common, Fonts, Gutters, Layout, Colors, darkMode } = useTheme()

  const [showPass, setShowPass] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()

  return (
    <SafeAreaView style={[Layout.fill]}>
      {/* <Spacer height={20} /> */}
      <View style={[Layout.fill, Common.backgroundPrimary]}>
        <ScrollView contentContainerStyle={[Gutters.padding16]}>
          <Spacer height={20} />
          <View>
            <Text style={[Fonts.title, Gutters.margin16B]}>Login</Text>
            <Text style={[Fonts.subtitle]}>silakan login terlebih dahulu</Text>
            <Spacer height={12} />
            <Text style={[Fonts.bodyBold]}>Email</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="example@email.com"
              onChangeText={setEmail}
            />
            <Spacer height={12} />
            <Text style={[Fonts.bodyBold]}>Password</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="Password"
              secureTextEntry={!showPass}
              onChangeText={setPassword}
              suffix={
                <MaterialIcons
                  name={showPass ? 'visibility-off' : 'visibility'}
                  size={20}
                  color={Colors.colorText.secondary}
                  style={Gutter.padding8H}
                  onPress={() => {
                    setShowPass(v => !v)
                  }}
                />
              }
            />

            <Spacer height={24} />
            <ButtonPrimary
              text="Login"
              onPress={() => {
                Snackbar.show({
                  text: 'Password tidak sesua',
                  backgroundColor: Colors.bgModal,
                  textColor: 'white',
                  duration: Snackbar.LENGTH_SHORT,
                })
              }}
            />
            <Spacer height={12} />
          </View>

          <Spacer height={120} />

          <View style={Layout.colCenter}>
            <Text style={Fonts.body}>{'Belum mempunyai akun?'}</Text>
            <TextButton
              text={'Daftar Sekarang'}
              onPress={() => {
                dispatch(
                  login({
                    user: {
                      email: faker.internet.email(),
                      username: faker.internet.userName(),
                    },
                  }),
                )
                navigation.navigate('Register')
              }}
            />
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default LoginScreen
