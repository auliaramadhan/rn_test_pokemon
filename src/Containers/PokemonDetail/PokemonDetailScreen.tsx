import React, { useState } from 'react'
import {
  View,
  ScrollView,
  SafeAreaView,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { useTheme } from '@/Hooks'
import { changeTheme, ThemeState } from '@/Store/Theme'
import { logOut } from '@/Store/auth'
import { Gutter } from '@/Theme/Gutters'
import { RootStackScreenProps } from '@/Navigators/utils'
import { ButtonPrimary, Divider, PokemonBadge, Spacer } from '@/Components'
import { useFetchPokemonDetailQuery } from '@/Services/modules/pokemon'
import myPokemon, { addPokemonToList, removePokemon } from '@/Store/myPokemon'
import { rootState } from '../../Store/index'
import { PokemonComponent } from './PokemonComp'
import { Images } from '@/Theme/Images'
import apiUrl from '@/Config/apiUrl'
import Snackbar from 'react-native-snackbar'

const PokemonDetailScreen: React.FC<RootStackScreenProps<'PokemonDetail'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors } = useTheme()

  const dispatch = useDispatch()

  const myPokemon = useSelector((state: rootState) => state.myPokemon)
  const isFavorit = useSelector((state: rootState) =>
    state.myPokemon.list.some(item => item.id === route.params.id),
  )

  const { data, isSuccess, isLoading, isFetching, error } =
    useFetchPokemonDetailQuery(route.params.id)

  const getColor = (type: string) => {
    if (['fire', 'dragon'].includes(type)) {
      return Colors.name.red + 'bb'
    } else if (['normal', 'fighting', 'flying', 'ghost'].includes(type)) {
      return Colors.name.grey + 'bb'
    } else if (['poison', 'bug', 'grass'].includes(type)) {
      return Colors.name.green + 'bb'
    } else if (['ground', 'rock', 'steel'].includes(type)) {
      return Colors.name.black + 'bb'
    } else if (['water', 'ice'].includes(type)) {
      return Colors.name.blue + 'bb'
    } else if (['electric', 'fairy', 'psychic'].includes(type)) {
      return Colors.name.yellow + 'bb'
    } else if (['dark', 'shadow', 'unknown'].includes(type)) {
      return Colors.name.black + 'bb'
    }
    return Colors.name.black + 'bb'
  }

  React.useEffect(() => {
    navigation.setOptions({
      headerTitleStyle: [Fonts.white], 
      headerTintColor: Colors.white,
    })
  }, [])
  
  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      {/* <Image source={Images.bgEllipse} style={styles.overlay} /> */}
      <View style={Layout.fill}>
        <ScrollView>
          <View
            style={[
              {
                backgroundColor: getColor(data?.types[0].type.name ?? ''),
              },
              Gutter.padding16,
            ]}
          >
            <Spacer height={60} />
            <View style={[Layout.absoluteCenter, Layout.center, { top: 20, bottom: -80 }]}>
              <Image
                resizeMode="contain"
                style={[Layout.fill, { height: 160, width: 160 }]}
                source={{
                  uri: `${apiUrl.BASE_IMG_POKE}/${data?.id}.png`,
                }}
              />
            </View>
            <Image
              source={Images.pokeball}
              style={[
                { position: 'absolute', right: -20, bottom: -20 },
                { height: 80, width: 80 },
              ]}
            />
            <View style={[Layout.rowSpaceBetween]}>
              <View>
                <Text style={[Fonts.title18Bold, Fonts.white]}>
                  {data?.name}
                </Text>
                <Spacer height={8} />
                <View style={[Layout.rowHCenter]}>
                  {data?.types.map((pokemon, i) => (
                    <PokemonBadge
                      name={pokemon.type.name}
                      style={Gutter.margin8R}
                    />
                  ))}
                </View>
                <Spacer height={8} />
              </View>
              <Text style={[Fonts.title18Bold, Fonts.white]}>
                #{String(data?.id).padStart(4, '0')}
              </Text>
            </View>
          </View>

          <View
            style={[
              Layout.fill,
              Common.topRadius(16),
              Gutter.padding16,
              Gutter.padding24T,
            ]}
          >
            <Text style={[Fonts.title16]}>About</Text>
            <Spacer height={4} />
            <Divider horizontal thick={2} />
            <Spacer height={16} />
            {!!data && <PokemonComponent.PokemonAboutComp pokemon={data!} />}

            <Spacer height={16} />
            <Text style={[Fonts.title16]}>Stat</Text>
            <Spacer height={4} />
            <Divider horizontal thick={2} />
            <Divider />
            <Spacer height={16} />
            {!!data && <PokemonComponent.PokemonStatComp stat={data!.stats} />}
          </View>

          <ButtonPrimary
            text={isFavorit ? 'Release' : 'Capture'}
            isLoading={isFetching || myPokemon.isLoading}
            onPress={async () => {
              // dispatch(changeTheme({ darkMode: true }))
              if (isFavorit) {
                dispatch(removePokemon(route.params))
              } else {
                try {
                  const data = await dispatch(addPokemonToList(route.params)).unwrap()
                  console.log(data)
                  Snackbar.show({
                    text: `Sukses Capture ${route.params.name}`,
                    numberOfLines: 2,
                    duration: Snackbar.LENGTH_LONG,
                  })
                  // eslint-disable-next-line no-catch-shadow, @typescript-eslint/no-shadow
                } catch (error: Error) {
                  console.log(error)
                  Snackbar.show({
                    text: `Gagal Capture: ${error.message}`,
                    numberOfLines: 2,
                    duration: Snackbar.LENGTH_LONG,
                  })
                }
              }
            }}
          />
        </ScrollView>
        {(isLoading || myPokemon.isLoading) && (
          <ActivityIndicator style={[Layout.absoluteCenter]} size="large" />
        )}
      </View>
    </SafeAreaView>
  )
}

export default PokemonDetailScreen
