/* eslint-disable react-native/no-inline-styles */
import { View, Text } from 'react-native'
import React from 'react'
import { useTheme } from '@/Hooks'
import { Pokemon, Species } from '../../types/api/pokemon.asd'
import { Spacer } from '@/Components'
import { Stat } from '@/types/api/pokemon'

const PokemonAboutComp = ({ pokemon }: { pokemon: Pokemon }) => {
  const { Colors, Fonts, Layout, Common, darkMode } = useTheme()
  return (
    <View>
      <View style={Layout.rowCenter}>
        <Text style={[Fonts.body, Layout.fill]}>Species</Text>
        <Text style={[Fonts.body, { flex: 3 }]}>{pokemon.species.name}</Text>
      </View>
      <Spacer height={4} />
      <View style={Layout.rowCenter}>
        <Text style={[Fonts.body, Layout.fill]}>Height</Text>
        <Text style={[Fonts.body, { flex: 3 }]}>{pokemon.height / 10} m</Text>
      </View>
      <Spacer height={4} />
      <View style={Layout.rowCenter}>
        <Text style={[Fonts.body, Layout.fill]}>Weight</Text>
        <Text style={[Fonts.body, { flex: 3 }]}>{pokemon.weight / 100} kg</Text>
      </View>
      <Spacer height={4} />
      <View style={Layout.rowCenter}>
        <Text style={[Fonts.body, Layout.fill]}>Abilities</Text>
        <Text style={[Fonts.body, { flex: 3 }]}>
          {pokemon.abilities.map(e => e.ability.name).join(', ')}
        </Text>
      </View>
    </View>
  )
}

const PokemonStatComp = ({ stat: stats }: { stat: Stat[] }) => {
  const { Colors, Fonts, Layout, Common, darkMode } = useTheme()
  const width = React.useMemo(() => Layout.getNumberWidth - 160, [])
  return (
    <View>
      {stats.map(stat => (
        <>
          <View style={Layout.rowCenter}>
            <Text style={[Fonts.body, Layout.fill]}>
              {stat.stat.name.toUpperCase()}
            </Text>
            <View style={{ width: width, height: 10 }}>
              <View
                style={[
                  {
                    height: 10,
                    width: (stat.base_stat / 100) * width,
                    backgroundColor:
                      stat.base_stat / 100 > 0.5
                        ? Colors.name.green
                        : Colors.name.red,
                  },
                ]}
              />
              <View
                style={[
                  Layout.absoluteCenter,
                  {
                    height: 10,
                    width: width,
                    backgroundColor: Colors.name.border1,
                  },
                ]}
              />
            </View>
          </View>
          <Spacer height={16} />
        </>
      ))}
    </View>
  )
}

export const PokemonComponent = {
  PokemonAboutComp,
  PokemonStatComp,
}
