import apiUrl from '@/Config/apiUrl'
import { api } from '@/Services/api'
import { Pokemon } from '@/types/api/pokemon'
import { PokemonList } from '@/types/api/pokemonList'

export const userApi = api.injectEndpoints({
  endpoints: build => ({
    fetchPokemon: build.query<PokemonList, requestList>({
      query: req => ({
        url: apiUrl.POKEMON,
        params: req,
      }),
      transformResponse(baseQueryReturnValue: PokemonList, meta, arg) {
        baseQueryReturnValue.results.forEach(pokemon => {
          // eslint-disable-next-line prettier/prettier
          pokemon.id = parseInt(pokemon.url.split('/')[6])
        })
        return baseQueryReturnValue
      },
    }),
    fetchPokemonDetail: build.query<Pokemon, number>({
      query: id => `${apiUrl.POKEMON}/${id} `,
    }),
  }),
  overrideExisting: false,
})

interface requestList {
  offset: number
  limit: number
}

export const { useFetchPokemonQuery, useFetchPokemonDetailQuery } = userApi
