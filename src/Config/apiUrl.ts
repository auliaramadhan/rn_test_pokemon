export default {
  BASE_URL: 'https://pokeapi.co/api/v2',
  POKEMON: '/pokemon',
  BASE_IMG_POKE: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon',
}
