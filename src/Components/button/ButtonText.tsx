import { useTheme } from '@/Hooks'
import React from 'react'
import {
  TouchableWithoutFeedback,
  Text,
  TouchableWithoutFeedbackProps,
  TextStyle,
} from 'react-native'

type IProp = TouchableWithoutFeedbackProps & {
  text: string
  style?: TextStyle
  disabled?: boolean
}

const TextButton : React.FC<IProp> = (props) => {
  const { style, text, disabled, ...arg } = props

  const {Colors, Fonts, Layout, Common, Images, darkMode } = useTheme()

  return (
    <TouchableWithoutFeedback {...arg} disabled={disabled}>
      <Text style={[Common.button.buttonText, style]}>{text}</Text>
    </TouchableWithoutFeedback>
  )
}

export default TextButton
