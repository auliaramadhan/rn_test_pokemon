import { StyleSheet, Text, TextStyle, TouchableOpacity, TouchableOpacityProps, View } from 'react-native'
import React, { ReactNode } from 'react'
import { useTheme } from '@/Hooks'

type IProp = TouchableOpacityProps & {
  text: string
  children?: ReactNode
  enabled: boolean
  textStyle?: TextStyle
}
const ButtonEnable: React.FC<IProp> = (props) => {
  const { text, textStyle, enabled, ...arg } = props

  const {Colors, Fonts, Layout, Common, Images, darkMode } = useTheme()

  const styles = StyleSheet.create({
    button: {
      padding : 0,
      borderRadius: 5,
      justifyContent: 'center',
      elevation: 0,
      flex:0,
    },
    buttonText: {
      color: Colors.colorText.primary,
      textAlign: 'center',
      alignSelf: 'center',
      margin: 8,
    },
    enableButtonText: {
      color: Colors.primary
    }
  })

  return (
    <TouchableOpacity {...arg}>
      <View
        style={[
          {
            backgroundColor: enabled ? Colors.bgPrimary : Colors.transparent,
          },
          Common.cardBorder,
          styles.button,
        ]}
      >
        <Text numberOfLines={1} style={[styles.buttonText, textStyle, enabled && styles.enableButtonText]}>
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  )
}

export default ButtonEnable