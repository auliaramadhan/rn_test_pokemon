/* eslint-disable @typescript-eslint/no-unused-vars */
import { useTheme } from '@/Hooks'
import { Layout } from '@/Theme'
import { Gutter } from '@/Theme/Gutters'
import Variables from '@/Theme/Variables'
import React, { ReactNode } from 'react'
import {
  Image,
  ImageSourcePropType,
  StyleSheet,
  Text,
  TextInput,
  TextInputProps,
  View,
  ViewStyle,
  TouchableOpacity,
} from 'react-native'

type IProp = TextInputProps & {
  prefixText?: string
  prefixIcon?: ImageSourcePropType
  prefixStyle?: ViewStyle
  prefixPress?: () => void
  prefix?: ReactNode
  suffixText?: string
  suffixIcon?: ImageSourcePropType
  suffixStyle?: ViewStyle
  suffixPress?: () => void
  suffix?: ReactNode
  containerStyle?: ViewStyle
}

const InputBorder: React.FC<IProp> = (props: IProp) => {
  const {
    prefixText,
    prefixIcon,
    prefixStyle,
    prefix,
    suffixText,
    suffixIcon,
    suffixStyle,
    suffix,
    prefixPress,
    suffixPress,
    containerStyle,
    ...argData
  } = props

  const { Fonts, Colors, Layout } = useTheme()

  const style = styles(Colors)

  const prefixComponent = () => {
    let child = prefix
    if (!!prefixText) {
      child = <Text style={[style.prefix, Fonts.body, Fonts.bold]} />
    } else if (!!prefixIcon) {
      child = (
        <Image source={prefixIcon} style={[style.image]} resizeMode="contain" />
      )
    }
    return (
      <TouchableOpacity onPress={prefixPress}>
        <View style={[style.prefix, prefixStyle]}>{child}</View>
      </TouchableOpacity>
    )
  }
  const suffixComponent = () => {
    let child = suffix
    if (!!suffixText) {
      child = <Text style={[style.suffix, Fonts.body, Fonts.bold]} />
    } else if (!!suffixIcon) {
      child = (
        <Image source={suffixIcon} style={style.image} resizeMode="contain" />
      )
    }
    return (
      <TouchableOpacity onPress={suffixPress}>
        <View style={[style.suffix, suffixStyle]}>{child}</View>
      </TouchableOpacity>
    )
  }

  return (
    <View
      style={[
        {
          borderWidth: 1,
          borderColor: Colors.divider,
          flexDirection: 'row',
          // flex : 1,
          alignItems: 'center',
          backgroundColor: Colors.white,
          borderRadius: 10,
        },
        containerStyle,
      ]}
    >
      {prefixComponent()}
      <TextInput
        {...argData}
        style={[Fonts.body, Layout.fill]}
        placeholderTextColor={Colors.colorText.secondary}
      />
      {suffixComponent()}
    </View>
  )
}

export default InputBorder

const styles = (Colors: typeof Variables.Colors) =>
  StyleSheet.create({
    inputContainer: {
      // flex: 1,
      borderWidth: 1,
      borderColor: Colors.divider,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: Colors.white,
      borderRadius: 10,
      // height:100,
    },
    prefix: {
      flex: 0,
      paddingLeft: 8,
    },
    suffix: {
      flex: 0,
      paddingRight: 8,
    },
    image: {
      alignSelf: 'center',
      height: 32,
      width: 32,
    },
  })
