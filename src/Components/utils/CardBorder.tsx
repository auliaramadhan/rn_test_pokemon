import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextStyle,
  View,
} from 'react-native'
import React from 'react'
import { Gutter } from '@/Theme/Gutters'
import { useTheme } from '@/Hooks'
import TextButton from '../button/ButtonText'

type Iprops = {
  title: string
  titleStyle?: TextStyle
  subtitle: string
  valueStyle?: TextStyle
  buttonText: string
  fetching: boolean
  onPress?: () => void
}

const CardBorder: React.FC<Iprops> = ({
  title,
  subtitle,
  onPress = () => null,
  buttonText = 'Lihat Detail',
  fetching = false,
  titleStyle,
  valueStyle,
}) => {
  const {Colors, Fonts, Layout, Common, Images, darkMode } = useTheme()

  return (
    <View
      style={[
        Common.cardBorder,
        {
          padding: 16,
          marginRight: 8,
          justifyContent: 'space-around',
          height: 102,
        },
      ]}
    >
      {fetching && (
        <ActivityIndicator
          animating={true}
          color={Colors.primary}
          style={Layout.absoluteCenter}
        />
      )}
      <View>
        <Text style={[Fonts.title, titleStyle]}>{title}</Text>
        {/* <View style={[Layout.rowCenter, Layout.justifyContentBetween]}>
         <TouchableWithoutFeedback onPress={showInfo}>
           <Image source={Images.Icon_Info} style={{height:14, width:14}}/>
         </TouchableWithoutFeedback>
       </View> */}
      </View>
      <View style={[Layout.rowCenter, Gutter.margin4]}>
        <Text style={[Fonts.body, valueStyle]}>{subtitle}</Text>
      </View>
      <View style={[Layout.rowHCenter]}>
        <TextButton onPress={onPress} text={buttonText} />
        <View style={{ width: 8 }} />
        {/* <MaterialIcons name='chevron' height={12} color={Colors.primary} /> */}
      </View>
    </View>
  )
}

export default CardBorder