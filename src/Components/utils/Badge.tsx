import { StyleSheet, Text, View, ViewStyle } from 'react-native'
import React from 'react'
import { useTheme } from '@/Hooks'
import { ViewProps } from 'react-native-svg/lib/typescript/fabric/utils'

const BadgePokemon: React.FC<IBadge> = props => {
  const { Colors, Fonts, Layout, Common, darkMode } = useTheme()
  const { name, style,...prop } = props
  return (
    <View style={[styles.badge, style]} {...prop}>
      <Text style={[Fonts.bodyBold, Fonts.white]}>{props.name}</Text>
    </View>
  )
}

export default BadgePokemon

interface IBadge extends ViewProps {
  name: string
}

const styles = StyleSheet.create({
  badge: {
    borderRadius: 100,
    backgroundColor: '#ffffff22',
    padding: 4,
    paddingHorizontal: 16,
  },
})
