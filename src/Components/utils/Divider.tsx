import { View, ViewStyle, ColorValue } from 'react-native';
import React from 'react'
import { useTheme } from '@/Hooks';

type Iprop = {
   horizontal?: boolean;
   vertical?: boolean;
   thick?: number;
   color?: ColorValue;
   style?: ViewStyle;
}

const Divider: React.FC<Iprop> = (props) => {

  const {Colors } = useTheme()

   const horizontal: ViewStyle = ({
      width: '100%',
      borderBottomWidth: props.thick,
      borderColor: props.color ?? Colors.divider,
   })
   const vertical: ViewStyle = ({
      height: '100%',
      borderLeftWidth: props.thick,
      borderColor: props.color ?? Colors.divider,
   })
   return (
      <View style={[props.horizontal && horizontal, props.vertical && vertical, props.style]} />
   )
}

Divider.defaultProps = {
   horizontal: false,
   vertical: false,
   thick: 1,
}

export default React.memo(Divider)