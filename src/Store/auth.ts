import { createSlice } from '@reduxjs/toolkit'

const slice = createSlice({
  name: 'auth',
  initialState: { isLogin: false } as UserState,
  reducers: {
    login: (state, payload: Payload) => {
      state.isLogin = payload.payload.isLogin ?? state.isLogin
      state.user = payload.payload.user
    },
    logOut: (state, _) => {
      state.isLogin = false
      state.user = undefined
    },
  },
})

export const { login, logOut } = slice.actions

export default slice.reducer

export type UserState = {
  isLogin: boolean
  user?: {
    email: string
    username: string
  }
}

type Payload = {
  payload: Partial<UserState>
  type: string
}
