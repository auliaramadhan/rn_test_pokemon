import { PokemonData } from '@/types/api/pokemonList'
import { delay } from '@/utils/delayer'
import { createAsyncThunk } from '@reduxjs/toolkit'

// First, create the thunk
export const addPokemonToList = createAsyncThunk(
  'pokemon/addPokemon',
  async (pokemon: PokemonData, thunkAPI) => {
    await delay(1000)
    const random = Math.random()
    if (random > 0.5) {
      throw new Error('Fail Capture')
    }
    return pokemon
  },
)
