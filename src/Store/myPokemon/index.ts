import { PokemonData } from '@/types/api/pokemonList'
import { Action, createSlice } from '@reduxjs/toolkit'
import { userApi } from '../../Services/modules/users/index'
import { addPokemonToList } from './thunk'

const slice = createSlice({
  name: 'myPokemon',
  initialState: {
    isLoading: false,
    isError: false,
    list: [],
    errorMessage: null,
  } as unknown as MyPokemonState,
  reducers: {
    removePokemon: (state, action: { payload: PokemonData }) => {
      const index = state.list.findIndex(e => e.id == action.payload.id)
      state.list.splice(index)
    },
  },
  extraReducers(builder) {
    builder.addCase(addPokemonToList.fulfilled, (state, action) => {
      if (state.list.some(v => v.id === action.payload.id)) {
        state.isError = true
        state.errorMessage = 'Pokemon sudah ada dalam list'
      } else {
        state.list.push(action.payload)
      }
      state.isLoading = false
    })
    builder.addCase(addPokemonToList.rejected, (state, action) => {
      state.isLoading = false
      state.isError = true
      state.errorMessage = action.error.message
    })
    builder.addCase(addPokemonToList.pending, (state, action) => {
      state.isError = false
      state.isLoading = true
    })
  },
})

export { addPokemonToList }
export const { removePokemon } = slice.actions
export default slice.reducer

export type MyPokemonState = {
  isLoading: boolean
  list: PokemonData[]
  isError: boolean
  errorMessage?: string
  errorData?: any
}

type Payload = {
  payload: Partial<MyPokemonState>
  type: string
}
