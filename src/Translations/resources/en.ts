export default {
  welcome: 'Welcome to React Native React_Native_Starter by TheCodingMachine',
  actions: {
    continue: 'Continue',
  },
  example: {
    helloUser: 'I am a fake user, my name is {{name}}',
    labels: {
      userId: 'Enter a user id',
    },
  },
}
