/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, EffectCallback } from 'react'

export default function useUpdateEffect(
  callback: EffectCallback,
  dependencies: any[],
) {
  const firstRenderRef = useRef(true)

  useEffect(() => {
    if (firstRenderRef.current) {
      firstRenderRef.current = false
      return
    }
    return callback()
  }, dependencies)
}
