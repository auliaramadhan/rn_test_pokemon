import { number } from 'prop-types'
import { useRef, useState } from 'react'

// type ToggleType<T> = boolean | [T, T]

// ada masalah sedikit

export default function useToggle<T>(initialData: boolean | [T, T] = false) {
  const sad =
    typeof initialData === 'boolean' ? initialData : (initialData[0] as T)
  const [value, setValue] = useState(sad)

  function toggleValue(valueData?: boolean | [T, T]) {
    if (typeof valueData === 'boolean') {
      setValue(currentValue => !currentValue)
    } else {
      setValue(currentValue =>
        currentValue === (initialData as T[])[0]
          ? (initialData as T[])[1]
          : (initialData as T[])[0],
      )
    }
  }

  return [value, toggleValue]
}
const [value, toggle] = useToggle(['asdas', 'asdas'])
