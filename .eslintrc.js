module.exports = {
  env: {
    'jest/globals': true,
  },
  root: true,
  extends: ['@react-native-community'],
  plugins: ['jest'],
  rules: {
    semi: ['error', 'never'],
    'object-curly-spacing': ['error', 'always'],
    'array-bracket-spacing': ['error', 'never'],
    'react/default-props-match-prop-types': ['error'],
    'react/sort-prop-types': ['error'],
    'no-unused-vars': 1,
    '@typescript-eslint/no-unused-vars': 1,
    'react/require-default-props': 1,
  },
  settings: {
    'import/resolver': {
      'babel-module': {},
    },
  },
}
